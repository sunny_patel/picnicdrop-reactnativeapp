import React from 'react';
import { StyleSheet, View, Text, AccessibilityInfo, Dimensions } from 'react-native';
import { Navigation } from 'react-native-navigation';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import debounce from 'lodash';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.05;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class Map extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            urlTemplate: 'http://c.tile.openstreetmap.org/{z}/{x}/{y}.png',
            region: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO
            },
            button: {
                buttonRadius: 100,
                style: {
                }
            }
        }
    }

    componentDidMount() {
        
    }

    openEventDetailsModel() {
        Navigation.showModal({
            screen: 'PicnicDrop.EventDetailsModal', // unique ID registered with Navigation.registerScreen
            title: 'Modal', // title of the screen as appears in the nav bar (optional)
            passProps: {}, // simple serializable object that will pass as props to the modal (optional)
            navigatorStyle: {}, // override the navigator style for the screen, see "Styling the navigator" below (optional)
            navigatorButtons: {}, // override the nav buttons for the screen, see "Adding buttons to the navigator" below (optional)
            animationType: 'slide-up' // 'none' / 'slide-up' , appear animation for the modal (optional, default 'slide-up')
          });
    }

    onMapReady(e) {
        navigator.geolocation.getCurrentPosition((pos) => {

            this.setState({
                ...this.state,
                region: {
                    ...this.state.region,
                    latitude: pos.coords.latitude,
                    longitude: pos.coords.longitude
                }
            });
            this._map.animateToRegion(this.state.region);
        });
        
    }

    render() {
        return (
        <View style={{flex: 1}}>
            <View style={{
                flex: 1
            }}>
                <MapView
                    ref={component => {this._map = component;}}
                    style={styles.container}
                    provider={PROVIDER_GOOGLE}
                    initialRegion={this.state.region}
                    showsUserLocation={false}
                    showsMyLocationButton={false}
                    showsPointsOfInterest={true}
                    onMapReady={this.onMapReady.bind(this)}
                    onRegionChange={this.props.onRegionChange}
                    onRegionChangeComplete={this.props.onRegionChangeComplete}
                >
                    <MapView.UrlTile
                        urlTemplate={this.state.urlTemplate}
                    />
                    
                </MapView>
            </View>

        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
