import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import colors from '../styles/colors';

export default class SearchMapResult extends React.Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {
        
    }


    render() {
        return (
        <View style={styles.container}>
            <Text>{this.props.name}</Text>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
        paddingTop: 10,
        paddingBottom: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
