let instance = null;

class MapsService {
    apiKey = 'AIzaSyAIUKLy35Zq1jrzsVNQjdtg8sO79UCNnx4';  
    constructor() {
        if (!instance) {
            instance = this;
        }
        return instance;
    }

    getAddressByLatLong(lat, long) {
        return fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${long}&key=${this.apiKey}`)
        .then((response) => response.json())
        .then((data)=> {
            if (Array.isArray(data.results) && data.results.length > 0) {
                return data.results[0].formatted_address;
                // return formatAddress(data.results[0].address_components);
            }
            throw false;
        });
    }

    getPlacesByQuery(query, lat, long) {
        query = query.replace(' ', '+');
        const radius = 10000;
        return fetch(
            `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${query}&location=${lat},${long}&radius=${radius}&key=${this.apiKey}`
        ).then((resp) => resp.json())
        .then((data) => {
            if (Array.isArray(data.results) && data.results.length > 0) {
                return data.results.map((place) => {
                    return {
                        id: place.id,
                        name: place.name,
                        address: place.formatted_address,
                        icon: place.icon
                    }
                });
            }
            throw [];
        });
    }
}

formatAddress = (addressComponents) => {
    let premise = findTypeOfAddrComponent('premise', addressComponents);
    premise = premise ? premise + ',' : '';
    
    let street_number = findTypeOfAddrComponent('street_number', addressComponents);
    let route = findTypeOfAddrComponent('route', addressComponents);
    let streetAddress = (route) ? `${street_number} ${route},` : '';

    let neighborhood = findTypeOfAddrComponent('neighborhood', addressComponents);
    neighborhood = (neighborhood) ? neighborhood + ',' : '';

    let formattedAddress = `${premise} ${streetAddress} ${neighborhood}`;
    return formattedAddress.substr(0, formattedAddress.length - 1);
};

findTypeOfAddrComponent = (type, components)  => {
    const foundComponent = components.find((component) => {
        return component.types.find((compType) => {
            return type == compType;
        });
    });

    if (foundComponent) {
        return foundComponent.short_name;
    }
    return '';
};

export default MapsService;