import {Navigation, ScreenVisibilityListener} from 'react-native-navigation';
import SearchMap from './SearchMap';
import Welcome from "./Welcome";
import EventDetailsModal from '../modals/EventDetailsModal';
import SearchResultsLightbox from '../lightboxes/SearchResultsLightbox';

export function registerScreens() {
    Navigation.registerComponent('PicnicDrop.Welcome', () => Welcome);
    Navigation.registerComponent('PicnicDrop.SearchMap', () => SearchMap);
    Navigation.registerComponent('PicnicDrop.EventDetailsModal', () => EventDetailsModal);
    Navigation.registerComponent('PicnicDrop.SearchResultsLightbox', () => SearchResultsLightbox);
}

export function registerScreenVisibilityListener() {
    new ScreenVisibilityListener({
      willAppear: ({screen}) => console.log(`Displaying screen ${screen}`),
      didAppear: ({screen, startTime, endTime, commandType}) => console.log('screenVisibility', `Screen ${screen} displayed in ${endTime - startTime} millis [${commandType}]`),
      willDisappear: ({screen}) => console.log(`Screen will disappear ${screen}`),
      didDisappear: ({screen}) => console.log(`Screen disappeared ${screen}`)
    }).register();
}
  