import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';
import Map from '../components/Map';
import { Icon, Button } from 'react-native-elements';
import colors from '../styles/colors';
import CommonStyles from '../styles/CommonStyles';
import MapsService from '../services/MapsService';
import * as _ from 'lodash';

export default class SearchMap extends React.Component {
    static navigatorStyle = {
        navBarHidden: true,
    };

    constructor(props) {
        super(props);

        this.onRegionChangeComplete = this.onRegionChangeComplete.bind(this);
        this.setSearchQuery = this.setSearchQuery.bind(this);
        this.clearSearch = this.clearSearch.bind(this);
        this.doSearchQuery = _.debounce(this.doSearchQuery, 1000).bind(this);

        this.state = {
            loading: true,
            region: {},
            addressSearchQuery: '',
            address: ''
        };

        this.mapsService = new MapsService();
    }

    toggleDrawer() {
        this.props.navigator.toggleDrawer({
            side: 'left', // the side of the drawer since you can have two, 'left' / 'right'
            animated: true, // does the toggle have transition animation or does it happen immediately (optional)
            to: 'open' // optional, 'open' = open the drawer, 'closed' = close it, missing = the opposite of current state
        });
    }

    clearSearch() {
        this.setSearchQuery('');
        this._searchQuery.focus();
    }

    setSearchQuery(query) {
        this.setState({
            ...this.state,
            addressSearchQuery: query
        }, () => {
            this.doSearchQuery();
        });
    }

    doSearchQuery() {
        if (this.state.addressSearchQuery.length < 5)
            return;
        this.setState({
            ...this.state,
            resultsLoading: true
        }, () => {
            this.mapsService.getPlacesByQuery(
                this.state.addressSearchQuery,
                this.state.region.latitude,
                this.state.region.longitude
            ).then((places) => {
                if (places.length > 1) {
                    this.props.navigator.showLightBox({
                        screen: 'PicnicDrop.SearchResultsLightbox',
                        style: {
                            tapBackgroundToDismiss: true
                        },
                        passProps: {
                            searchResults: places
                        }
                    });
                }
            }).catch((err) => {
                // animate shaking headerView
            }).finally(() => {
                this.setState({
                    ...this.state,
                    resultsLoading: false
                })
            });
        });
    }

    onRegionChangeComplete(region) {
        this.setState({
            ...this.state,
            region: {
                ...this.region,
                latitude: region.latitude,
                longitude: region.longitude
            }
        }, () => {
            this.mapsService.getAddressByLatLong(region.latitude, region.longitude)
            .then((address) => {
                this.setState({
                    ...this.state,
                    loading: false,
                    addressSearchQuery: address,
                    address
                }, () => {
                    this._searchQuery.blur();
                });
            }).catch(() => {});
        });
    }



    render() {
        textActionButton = () => {
            if (this.state.resultsLoading) {
                return (
                    <Icon
                        reverse
                        name='clear'
                        type='materialicons'
                        size={20}
                        onPress={this.clearSearch}
                        color='transparent'
                        reverseColor={colors.accent}
                        underlayColor='transparent'
                    />
                    
                );
            } else {
                return (
                    <Icon
                        reverse
                        name='clear'
                        type='materialicons'
                        size={20}
                        onPress={this.clearSearch}
                        color='transparent'
                        reverseColor={colors.secondary}
                        underlayColor='transparent'
                        loading={true}
                    />
                );
            }
        }
        return (
            <View style={styles.container}>
                <View
                    style={styles.map}>
                    <Map
                        onRegionChangeComplete={this.onRegionChangeComplete}
                    />
                </View>
                <View
                    style={styles.headerView}
                >
                    <Icon
                        reverse
                        name='ios-menu'
                        type='ionicon'
                        size={20}
                        onPress={this.toggleDrawer.bind(this)}
                        color='transparent'
                        reverseColor={colors.secondary}
                        underlayColor='transparent'
                    />
                    <TextInput
                        ref={component => {this._searchQuery = component;}}
                        style={{
                            flex: 1,
                            height: '100%',
                            fontSize: 16,
                            backgroundColor: 'transparent'
                        }}
                        value={this.state.addressSearchQuery}
                        onChangeText={this.setSearchQuery}
                    />
                    {textActionButton()}
                </View>
                <View
                    style={CommonStyles.center}
                >
                    <Icon
                        reverse
                        name='ios-pin'
                        type='ionicon'
                        size={40}
                        color='transparent'
                        reverseColor={colors.accent}
                        underlayColor='transparent'
                    />
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    map: {
        height: '100%',
        width: '100%'
    },
    headerView: {
        position: 'absolute',
        top: 35,
        width: '95%',
        flexDirection: 'row',
        backgroundColor: colors.primary,
        borderRadius: 100,
        alignItems: 'center',
        shadowColor: colors.secondary,
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2
    },
    footerBtns: {
        position: 'absolute',
        bottom: 20
    }
});
