import React from 'react';
import { Navigation } from 'react-native-navigation';
import {StyleSheet, View, Text, TextInput, TouchableHighlight, Animated, Dimensions} from 'react-native';
import Map from '../components/Map';
import { Icon, Button } from 'react-native-elements';

export default class EventTitleModal extends React.Component {
    static navigatorStyle = {
        navBarHidden: false,
    };

    constructor(props) {
        super(props);

        this.closeModal = this.closeModal.bind(this);
    }

    closeModal() {
        Navigation.dismissModal({
            animationType: 'slide-down'
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Button
                    title="Close"
                    onPress={this.closeModal}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    }
});
