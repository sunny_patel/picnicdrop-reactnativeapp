import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import { Icon, Button, colors, Text } from 'react-native-elements';
import SearchMapResult from '../components/SearchMapResult';

export default class SearchResultsLightbox extends React.Component {
    static navigatorStyle = {
        navBarHidden: false,
    };

    constructor(props) {
        super(props);

    }

    _keyExtractor = (item, index) => item.id;
    _renderHeader = () => (
        <Text h2>Results</Text>
    );

    _renderItem = ({item}) => (
        <SearchMapResult
            name={item.name}
        />
    );

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    keyExtractor={this._keyExtractor}
                    data={this.props.searchResults}
                    listHeaderComponent={this._renderHeader}
                    renderItem={this._renderItem}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
        alignItems: 'center'
    }
});
