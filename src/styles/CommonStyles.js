import {StyleSheet} from 'react-native';

export default CommonStyles = StyleSheet.create({
    center: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center'
    }
});