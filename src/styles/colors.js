export default colors = {
    primary: '#FAFAFA',
    secondary: '#37474F',
    accent: '#304FFE'
}