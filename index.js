import { Navigation } from 'react-native-navigation';
import {Platform} from 'react-native';
import {registerScreens, registerScreenVisibilityListener} from './src/screens';

registerScreens();
registerScreenVisibilityListener();

Navigation.startSingleScreenApp({
    screen: {
        screen: 'PicnicDrop.SearchMap',
    },
    drawer: {
        left: {
            screen: 'PicnicDrop.Welcome',
            fixedWidth: 500,
            disableOpenGesture: true
        },
        style: {
            drawerShadow: false
        },
        type: 'TheSideBar',
        animationType: 'door'
    }
});